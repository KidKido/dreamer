package com.dreamer.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dreamer.entities.Team;
import com.dreamer.services.TeamService;

@Controller
public class TeamController {

	@Autowired
	TeamService teamService;

	@RequestMapping(value = "/team.action", method = RequestMethod.GET)
	public ModelAndView getTeam(HttpServletRequest request) {
	
	ModelAndView mav = new ModelAndView("team");
	
	List<Team> teamList = teamService.getTeamList();
	mav.addObject("teamList", teamList);
	
	return mav;
	}
}
