package com.dreamer.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AboutSchoolController {

	@RequestMapping(value = "/features.action", method = RequestMethod.GET)
	public ModelAndView getFeatures(HttpServletRequest request) {
		return new ModelAndView();
	}

	@RequestMapping(value = "/intiative.action", method = RequestMethod.GET)
	public ModelAndView getInitiative(HttpServletRequest request) {
		return new ModelAndView();
	}

	@RequestMapping(value = "/vacancies.action", method = RequestMethod.GET)
	public ModelAndView getVacancies(HttpServletRequest request) {
		return new ModelAndView();
	}

	@RequestMapping(value = "/pricing.action", method = RequestMethod.GET)
	public ModelAndView getPricing(HttpServletRequest request) {
		return new ModelAndView();
	}

	@RequestMapping(value = "/video.action", method = RequestMethod.GET)
	public ModelAndView getVideo(HttpServletRequest request) {
		return new ModelAndView();
	}
}
