package com.dreamer.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
	@RequestMapping(value = "/index.action", method = RequestMethod.GET)
	public ModelAndView getIndex(HttpServletRequest request) {
		return new ModelAndView();
	}

}
