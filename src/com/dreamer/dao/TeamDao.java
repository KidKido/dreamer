package com.dreamer.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dreamer.entities.Team;

@Repository
public class TeamDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<Team> getTeamList() {
		return sessionFactory.getCurrentSession().createCriteria(Team.class).addOrder(Order.asc("id")).list();
	}
}
