package com.dreamer.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dreamer.dao.TeamDao;
import com.dreamer.entities.Team;

@Service
public class TeamService {
	
	@Autowired(required = false)
	TeamDao teamDao;

	@Transactional
	public List<Team> getTeamList() {
		return teamDao.getTeamList(); 
	}
}
