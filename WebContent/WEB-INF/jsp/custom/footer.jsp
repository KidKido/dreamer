<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<section class="content-block-nopad footer-wrap-1-1 bg-deepocean">
	<div class="container footer-1-1">
		<div class="row">
			<div class="col-sm-5">
				<img src="images/logo.png" class="brand-img img-responsive">
				<h3>
					<strong>Эпичный</strong> текстик о школе
				</h3>
				<p class="lead">Покупайте наших слонов!</p>
			</div>
			<div class="col-sm-6 col-sm-offset-1">
				<div class="row">
					<div class="col-md-4">
						<h4>
							Страницы<br>
						</h4>
						<ul>
							<li><a href="pricing.action">Цены</a></li>
							<li><a href="team.action">Наша команда</a></li>
							<li><a href="features.action">Наша приципы</a></li>
							<li><a href="vacancies.action">Вакансии</a></li>
							<li><a href="#">Контакты</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
</section>