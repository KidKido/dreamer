<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header id="header-1" class="soft-scroll header-1" data-pg-collapsed>
    <!-- Navbar -->
    <nav class="main-nav navbar-fixed-top headroom headroom--pinned bg-marina">
        <div class="container">
            <!-- Brand and toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.action">
                    <img src="images/logo223.png" class="brand-img img-responsive pad-bottom0 pad0">
                </a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item">
                        <a href="index.action">Главная<br></a>
                    </li>
                    <li class="nav-item">
</li>
                    <li class="nav-item">
                        <a href="pricing.action">цены</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">о нас&nbsp;<i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="features.action">Наши принципы</a>
                            </li>
                            <li>
                                <a href="team.action">Наша команда</a>
                            </li>
                            <li>
                                <a href="vacancies.action">Вакансии</a>
                            </li>
                            <li>
                                <a href="intiative.action">инициатива 2000</a>
                            </li>
                            <li>
                            	<a href="video.action">Наши видеоуроки</a>
                            </li>
                        </ul>                         
                    </li>
                    <!--//dropdown-->                     
                    <li class="nav-item">
                        <a href="#">контакты<br></a>
                    </li>
                </ul>
                <!--//nav-->
            </div>
            <!-- Navigation -->
            <!--// End Navigation -->
        </div>
        <!--// End Container -->
    </nav>
    <!--// End Navbar -->
</header>