<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
    <body data-spy="scroll" data-target="nav">
        <jsp:include page="custom/header.jsp" />
        <section id="content-1-9" class="content-1-9 content-block">
            <div class="container">
                <div class="underlined-title">
                    <h1>Вакансии</h1>
                    <hr>
                    <h2>Некоторые преимущества сотрудничества с «Ближе к мечте»</h2>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-money"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Возможности</h4>
                            <p>Наши преподаваетли зарабатывают 8-13 тысяч гривен в месяц.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-dollar"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Выплаты</h4>
                            <p>Зарплату мы выплачиваем не по окончании проекта, не ежемесячно, не два раза в месяц, а ЕЖЕНЕДЕЛЬНО.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-comments-o"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Занятость</h4>
                            <p>Поскольку мы – школа №1 Киева, у нас (значит, и у Вас, если Вы соответствуете нашим стандартам), никогда нет недостатка в студентах (они к нам идут толпами).</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-signal"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Развитие</h4>
                            <p>Вы развиваетесь постоянно, поскольку наш корпоративный язык общения – английский.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-map"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Расположение</h4>
                            <p>Мы очень выгодно расположены.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pad25">
                        <div class="col-xs-2">
                            <span class="fa fa-long-arrow-up"></span>
                        </div>
                        <div class="col-xs-10">
                            <h4>Рост</h4>
                            <p>Мы постоянно повышаем зарплату преподавателей. В среднем – на 10% в год.</p>
                        </div>
                    </div>
                </div>
                <section id="content-3-7" class="content-block content-3-7">
                    <div class="container">
                        <div class="col-sm-12">
                            <div class="underlined-title">
                                <h1>А вообще...</h1>
                                <hr>
                                <h2>Вам <span class="fa fa-heart pomegranate"></span> понравится у нас</h2>
                                <p>В Киеве есть два вида языковых школ: «Ближе к мечте» и все остальное. Кстати, Вы не читали отзывов о нашей школе на независимом портале Enguide, сравнивающим все школы английского Киева? Почитайте, Вы все поймете.</p>
                                <p>Высылайте  Ваше резюме на адрес  dreamerkyiv@gmail.com с пометкой «Преподаватель … языка».</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="content-3-7" class="content-block content-3-7">
                    <div class="container">
                        <div class="col-sm-12">
                            <div class="underlined-title">
                                <h1>Переводчики</h1>
                                <hr>
                                <h2>Так жы мы проводим постоянный набор переводчиков с разных языков</h2>
                                <p>Наличие диплома переводчика желательно – для возможности нотариального заверения перевода.</p>
                                <p>Высылайте Ваше резюме на электронный адрес svetlana@dreamer.kiev.ua с пометкой «Переводчик … языка».</p>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
		<jsp:include page="custom/footer.jsp" />
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>         
        <script type="text/javascript" src="js/bootstrap.min.js"></script>         
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/bskit-scripts.js"></script>         
    </body>     
</html>
