<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
    <body data-spy="scroll" data-target="nav">
        <jsp:include page="custom/header.jsp" />
        <section class="content-block team-2 team-2-1">
            <div class="container">
                <div class="underlined-title">
                    <h1>Наша команда<br></h1>
                    <hr>
                </div>
                <div class="row">
                    <div class="team-wrapper col-sm-4">
                        <div class="team-item">
                            <div class="team-thumb">
                                <img src="images/anya%20final%20resized.jpg" class="img-responsive" alt="Member Image">
                            </div>
                            <div class="team-details">
                                <h4><h3>Анна Интелегатор</h3></h4>
                                <h5><hr />Преподаватель английского, соучредитель</h5>
                            </div>
                        </div>
                    </div>
                    <div class="team-wrapper col-sm-4">
                        <div class="team-item">
                            <div class="team-thumb">
                                <img src="images/yura%20final%20resized.jpg" class="img-responsive" alt="Member Image">
                            </div>
                            <div class="team-details">
                                <h4><h3>Юрий Интелегатор</h3></h4>
                                <h5><hr />Преподаватель английского, учредитель</h5>
                            </div>
                        </div>
                    </div>
                    <c:forEach var = "each" items = "${teamList}">
                    <div class="team-wrapper col-sm-4">
                        <div class="team-item">
                            <div class="team-thumb">
                                <img src="images/profile/${each.getId()}.jpg" class="img-responsive" alt="Member Image">
                            </div>
                            <div class="team-details">
                                <h3>${each.getFirstName()} ${each.getLastName()}</h3>
                                <hr />
                                <h5>${each.getUserRole().getName()}<br><br></h5>
                            </div>
                        </div>
                    </div>
                    </c:forEach>
                    <!-- /.gallery-item-wrapper -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
		<jsp:include page="custom/footer.jsp" />
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>         
        <script type="text/javascript" src="js/bootstrap.min.js"></script>         
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/bskit-scripts.js"></script>         
    </body>     
</html>