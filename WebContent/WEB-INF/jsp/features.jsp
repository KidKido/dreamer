<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
    <body data-spy="scroll" data-target="nav">
        <jsp:include page="custom/header.jsp" />
        <div class="underlined-title pad5" style="font-family: 'Barrio', cursive;">
            <h1 style="font-family: 'Barrio', cursive;">Наши принципы</h1>
            <hr>
        </div>
        <section id="content-3-4" class="content-block content-3-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-1">
                        <img class="img-rounded img-responsive" src="images/certif.jpg">
                        <hr />
                        <h4 style="font-family: 'Alegreya SC'">Сертификат, который выдается 
школой "Ближе к мечте" 
по окончанию курса.</h4>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <div class="panel-group visible-sm-inline-block visible-md visible-lg visible-xs">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle" data-toggle="collapse" data-parent=".panel-group" href="#content1"><span>Отношение к студентам</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>Мы отдаем себе отчет, что наши студенты более важны, чем мы. Они доверяют нам, рассчитывают на нашу помощь, платят деньги. И мы не можем их подвести. Поэтому наше наибольшее опасение – не оправдать ожиданий тех, кого мы учим. Наша величайшая радость – когда люди получают от обучения больше, чем ожидали.</p>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content2"><span>Развитие разговорных навыков, или «Немым - говорить!»</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Грустно слышать, когда люди констатируют: «Учу английский уже больше года, но говорить по-прежнему не могу». Так быть не должно! Поэтому основной акцент, который мы делаем на наших занятиях – развитие разговорных навыков, независимо от уровня обучаемых. Безусловно, есть люди с разными лингвистическими способностями, но мы убеждены, что заговорить на иностранном языке может даже самый «безнадежный». На протяжении лет многие студенты говорили нам, что именно мы «разговорили» их. Как сказал один из наших учащихся: «У вас даже немой заговорит». Почему мы не боимся приводить эту цитату во всеуслышание? Потому что это правда.</p>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content3"><span>Постоянное развитие преподавателей</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Кто-то сказал: «Большинство людей, устраиваясь на работу, начинают… деградировать». Даже цифры приводят: 80%, 70% (а вы как думаете?). К сожалению, курсы иностранных языков – не исключение. Независимо от статистики, мы создали такие условия, при которых не развиваться профессионально невозможно. Преподаватели общаются друг с другом на иностранном языке, регулярно пишут тесты, делятся опытом, интересуются новинками в преподавании. Одним словом, если вы решаете работать с нашими преподавателями, они гарантированно являются одними из лучших специалистов языковых школ.</p>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content4"><span>Доступные цены</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Мы счастливы осознавать, что предлагаем обучение высокого уровня по доступным ценам. Мы убеждены, что курсы английского или любого другого языка не должны быть доступны лишь немногим избранным. Каждый имеет право мечтать о хорошем образовании, успешной работе, обеспеченном будущем. И каждый имеет право быть способным оплатить одно из необходимых условий этого – знание иностранных языков! Очень гордимся тем фактом, что в 2015 году мы были одной из немногих компаний, которая не повысила цены на свои услуги.</p>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                        </div>
                        <!-- /.accordion -->
                    </div>
                    <!-- /.column -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
        <jsp:include page="custom/footer.jsp" />
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>         
        <script type="text/javascript" src="js/bootstrap.min.js"></script>         
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/bskit-scripts.js"></script>         
    </body>     
</html>