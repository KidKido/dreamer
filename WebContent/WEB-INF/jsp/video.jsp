<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
<jsp:include page="custom/header.jsp" />
<section id="content-3-7" class="content-block content-3-7">
            <div class="container">
                <div class="col-sm-12">
                    <div class="underlined-title">
                        <h1 style="font-family: 'Barrio', cursive;">Наши видеоуроки</h1>
                        <hr>
                    </div>
                </div>
            </div>
        </section>
        <section id="content-3-4" class="content-block content-3-4">
            <div class="container">
                <div class="row">
                    <div>
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle" data-toggle="collapse" data-parent=".panel-group" href="#content1"><span>FUNNY GRAMMAR. Lesson 13</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/_NYLW5zoQMg?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content2"><span>FUNNY GRAMMAR. Lesson 12</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/L9Buy3WuSaw?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content3"><span>FUNNY GRAMMAR. Lesson 11</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/0ec_Q4w9i3Y?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content4"><span>FUNNY GRAMMAR. Lesson 10</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/RCXzKDs6qh4?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content5"><span>FUNNY GRAMMAR. Lesson 9</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/9Cs6FVHXHLc?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content6"><span>FUNNY GRAMMAR. Lesson 8</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/T0250fnoAZs?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content7"><span>FUNNY GRAMMAR. Lesson 7</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content7" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/-QWRDboPOSM?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content8"><span>FUNNY GRAMMAR. Lesson 6</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content8" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/w-aST4Fy4gs?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content9"><span>FUNNY GRAMMAR. Lesson 5</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content9" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/otuf5XG2OZQ?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content10"><span>FUNNY GRAMMAR. Lesson 4</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content10" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/chIdpF6EPTI?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content11"><span>FUNNY GRAMMAR. Lesson 3</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content11" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/nQBbjzKyypI?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content12"><span>FUNNY GRAMMAR. Lesson 2</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content12" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/4cElQygDvSM?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a class="panel-toggle collapsed" data-toggle="collapse" data-parent=".panel-group" href="#content13"><span>FUNNY GRAMMAR. Lesson 1</span></a></h4>
                                </div>
                                <!-- /.panel-heading -->
                                <div id="content13" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="vid">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/7SSAwXI7t3M?ecver=1" ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.content -->
                            </div>
                        </div>
                        <!-- /.accordion -->
                    </div>
                    <!-- /.column -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
        <jsp:include page="custom/footer.jsp" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/plugins.js"></script>
<script type="text/javascript" src="js/bskit-scripts.js"></script>
</body>
</html>