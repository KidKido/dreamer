<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
    <body data-spy="scroll" data-target="nav">
        <jsp:include page="custom/header.jsp" />
        <br />
        <div class="underlined-title">
            <h1 style="font-family: 'Barrio', cursive;">Цены</h1>
            <hr>
        </div>         
        <section class="content-block content-1-8">
            <div class="container">
                <ul class="nav nav-tabs text-center" role="tablist" id="myTab">
                    <li class="active">
                        <a href="#tab1" role="tab" data-toggle="tab">На територии школы</a>
                    </li>
                    <li>
                        <a href="#tab2" role="tab" data-toggle="tab">Корпоративные</a>
                    </li>
                    <li>
                        <a href="#tab3" role="tab" data-toggle="tab">Для школьников</a>
                    </li>
                    <li>
                        <a href="#tab4" role="tab" data-toggle="tab">для дошкольников</a>
                    </li>
                    <li>
                        <a href="#tab5" role="tab" data-toggle="tab">TOEFL/IELTS</a>
                    </li>
                    <li>
                        <a href="#tab6" role="tab" data-toggle="tab">Остальное</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">На територии школы</h1>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Группа</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">600</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>4 - 8 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индивидуальное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">160</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Семейное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">200</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>2 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индивидуальное<br>(с 18:00 - 21:00)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">240</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>2 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Семейное&nbsp;<br>(с 18:00 - 21:00)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">280</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>2 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab1 -->
                    <div class="tab-pane fade" id="tab2" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">Корпоративное обучение английскому</h1>
                                    <hr>
                                    <h2>На вашей територии</h2>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="price-block col-md-4">
                                            <header>
                                                <h2>Групповые<br><br></h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">3200</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="price-block col-md-4">
                                            <header>
                                                <h2>Групповые<br>(с 18:00 - 21:00)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">3800</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <!-- /.price-block -->
                                        <div class="price-block col-md-4">
                                            <header>
                                                <h2>Индивидуальное<br><br></h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">240</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features pad25 pad-bottom25">
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                        <!-- /.row -->
                    </div>
                    <!-- /#tab2 -->
                    <div class="tab-pane fade" id="tab3" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">Английский для школьников</h1>
                                    <hr>
                                    <h2>Занятия / подготовка к зно</h2>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Группа<br><br></h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">560</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>4 - 8 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индивидуальное<br><br></h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">160</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header class="pad-bottom30">
                                                <h2>Подготовка к ЗНО<br>(в группе)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">280</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>4 - 8 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Подготовка к ЗНО<br>(индивидуально)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">280</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Выездное<br><br></h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">240</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>50 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab3 -->
                    <div class="tab-pane fade" id="tab4" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">Английскоий для дошкольников</h1>
                                    <hr>
                                    <h2>и детей младшего школьного возраста</h2>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Группа</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">560</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>До 6 человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>45 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индивидуальное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">160</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>45 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Выездное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">240</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>45 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                    </div>
                    <div class="tab-pane fade" id="tab5" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">Подготовка к TOEFL/IELTS</h1>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Группа</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">1920</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>4 - 8 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>120 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индивидуальное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">240</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                    </div>
                    <div class="tab-pane fade" id="tab6" data-pg-collapsed>
                        <section class="content-block pricing-table-2">
                            <div class="container">
                                <div class="underlined-title">
                                    <h1 style="font-family: 'Barrio', cursive;">Остальные языки</h1>
                                    <hr>
                                    <h2>Немецкий, испанский, французский, итальянский, польский языки</h2>
                                </div>
                                <div class="row">
                                    <div class="tables-row">
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Группа</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">720</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>4 - 8 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индвидуальное</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">200</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Семейная группа</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">200</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>2 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Групповые<br>(выезд)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">3800</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>На ваше усмотрение
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>8 Занятий
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>90 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <div class="col-md-4 price-block">
                                            <header>
                                                <h2>Индвидуальное<br>
    (выезд)</h2>
                                                <div class="price">
                                                    <span class="currency">₴</span>
                                                    <span class="amount">280</span>
                                                </div>
                                                <!-- /.price -->
                                            </header>
                                            <ul class="features">
                                                <li>
                                                    <i class="fa fa-users fa-lg"></i>1 Человек
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil fa-lg"></i>1 Занятие
                                                </li>
                                                <li>
                                                    <i class="fa fa-clock-o fa-lg"></i>60 Минут
                                                </li>
                                            </ul>
                                            <!-- /.features -->
                                        </div>
                                        <!-- /.price-block -->
                                        <!-- /.price-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.container -->
                        </section>
                        <!-- /.row -->
                    </div>
                    <!-- /#tab4 -->
                </div>
                <!-- /.tab-content -->
            </div>
            <section class="content-block content-3-7" data-pg-collapsed>
                <div class="container" data-pg-collapsed>
                    <div class="col-sm-12" data-pg-collapsed>
                        <div class="underlined-title">
                            <h1 style="font-family: 'Barrio', cursive;">Дополнительная информация</h1>
                            <hr>
                            <h2>СКИДКИ <span class="fa pomegranate fa-percent"></span> и другие приятности!</h2>
                        </div>
                    </div>
                    <div class="col-sm-12" data-pg-collapsed>
                        <h5 class="text-center">Занятия в группах проходят два раза в неделю по два академических часа в  дни, согласованные с учащимися.</h5>
                        <h5 class="text-center">Частота и продолжительность индивидуальных занятий устанавливается в соответствии с вашими потребностями.</h5>
                        <h5 class="text-center">Занятия могут проходить с 7:30 утра до 9:00 вечера у нас в офисе (карту проезда можно посмотреть здесь: Контакты) либо на Вашей территории.</h5>
                        <h5 class="text-center">Рады Вам сообщить, что у родственников студентов нашей школы есть возможность заниматься с 10% скидкой в группе.</h5>
                        <h5 class="text-center">У наших студентов, занимающихся в группах на территории школы, есть возможность замораживать занятия. Таким образом, в течение одного курса/уровня студент может заморозить от 1 до 4 занятий 2 раза. Гордимся тем фактом, что мы были первой языковой школой Киева, которая ввела такую услугу.</h5> 
                    </div>
                </div>
            </section>
            <!-- /.container -->
        </section>
        <jsp:include page="custom/footer.jsp" />
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>         
        <script type="text/javascript" src="js/bootstrap.min.js"></script>         
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/bskit-scripts.js"></script>         
    </body>     
</html>