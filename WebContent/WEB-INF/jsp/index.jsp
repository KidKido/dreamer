<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="custom/head.jsp" />
<body data-spy="scroll" data-target="nav">
	<jsp:include page="custom/header.jsp" />
	<div class="page-header margin-top0 margin-bottom0">
		<div class="underlined-title pad5"
			style="font-family: 'Barrio', cursive;">
			<h1 class="text-center" style="font-family: 'Barrio', cursive;">Наши
				школы</h1>
			<hr>
		</div>
	</div>
	<div id="map" class="min-height-500px"></div>
	<section class="content-block pricing-table-2" data-pg-collapsed>
		<div class="container">
			<div class="underlined-title">
				<a href="pricing.action"><h1
						style="font-family: 'Barrio', cursive;">ЦЕНЫ</h1></a>
				<hr>
				<h2>На територии школы</h2>
			</div>
			<div class="row">
				<div class="tables-row">
					<div class="col-md-4 price-block">
						<header>
							<h2>
								группа<br>
							</h2>
							<div class="price">
								<span class="currency">₴</span> <span class="amount">600</span>
							</div>
							<!-- /.price -->
						</header>
						<ul class="features">
							<li><i class="fa fa-users fa-lg"></i>4 - 8 Человек</li>
							<li><i class="fa fa-pencil fa-lg"></i>8 Занятий</li>
							<li><i class="fa fa-clock-o fa-lg"></i>90 Минут</li>
						</ul>
						<!-- /.features -->
					</div>
					<div class="col-md-4 price-block">
						<header>
							<h2>
								индивидуальное<br>
							</h2>
							<div class="price">
								<span class="currency">₴</span> <span class="amount">160</span>
							</div>
							<!-- /.price -->
						</header>
						<ul class="features">
							<li><i class="fa fa-users fa-lg"></i>1 Человек</li>
							<li><i class="fa fa-pencil fa-lg"></i>1 Занятие</li>
							<li><i class="fa fa-clock-o fa-lg"></i>50 Минут</li>
						</ul>
						<!-- /.features -->
					</div>
					<div class="price-block col-md-4">
						<header>
							<h2>семейное</h2>
							<div class="price">
								<span class="currency">₴</span> <span class="amount">200<br></span>
							</div>
							<!-- /.price -->
						</header>
						<ul class="features">
							<li><i class="fa fa-users fa-lg"></i>2 Человека</li>
							<li><i class="fa fa-pencil fa-lg"></i>1 Занятие</li>
							<li><i class="fa fa-clock-o fa-lg"></i>60 Минут</li>
						</ul>
						<!-- /.features -->
					</div>
					<div class="col-md-4 price-block">
						<header>
							<h2>
								ИНДИВИДУАЛЬНОЕ<br>(с 18:00 - 21:00)
							</h2>
							<div class="price">
								<span class="currency">₴</span> <span class="amount">240<br></span>
							</div>
							<!-- /.price -->
						</header>
						<ul class="features">
							<li><i class="fa fa-users fa-lg"></i>1 Человека</li>
							<li><i class="fa fa-pencil fa-lg"></i>1 Занятие</li>
							<li><i class="fa fa-clock-o fa-lg"></i>50 Минут</li>
						</ul>
						<!-- /.features -->
					</div>
					<div class="col-md-4 price-block">
						<header>
							<h2>
								Семейное<br>(с 18:00 - 21:00)
							</h2>
							<div class="price">
								<span class="currency">₴</span> <span class="amount">280<br></span>
							</div>
							<!-- /.price -->
						</header>
						<ul class="features">
							<li><i class="fa fa-users fa-lg"></i>2 Человека</li>
							<li><i class="fa fa-pencil fa-lg"></i>1 Занятие</li>
							<li><i class="fa fa-clock-o fa-lg"></i>60 Минут</li>
						</ul>
						<!-- /.features -->
					</div>
					<!-- /.price-block -->
					<!-- /.price-block -->
					<!-- /.price-block -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container -->
	</section>
	<jsp:include page="custom/footer.jsp" />
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/plugins.js"></script>
	<script
		src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyBCVTY06yVtcukfbOQtvuAaNAgYsBLtvx4"></script>
	<script type="text/javascript" src="js/bskit-scripts.js"></script>
</body>
</html>
